# Script Name: tea_time_class_1.R
# Purpose: Learning R 
# Authors: Ethan Pickering 
# License: Creative Commons Attribution-NonCommercial-ShareAlike- 4.0 International License.  
# Latest Changelog Entires: v0.00.01 - tea_time_class1.R - Ethan Started it

# A pound symbol, or "Hashtag" indiactes a comment in the code

# Assigining Operators

## PRO TIP ## - Control enter runs the line you are on or a highlighted section

# Assign number


# Lets view it with function View()


# We can also look into what view is as a function and how it works
?View() # The question command shows us the help file

# Assign a vector or array of numbers, from 1 - 5


# We can also use = instead of <- (supposeduly <- is better than =, not sure why)
# Assign a character string


# Use print function to print to console



# R Object and type of classes
# Characters, integers, numeric, complix, Logic (TRUE/FALSE)

# R Vectors

c = c(1.23452, 2.1435, 3.14)
# The function c() here is "concatenate" so it will combine the values


# These will have attributes such as dimensions, object class, and length

# Lets check all of the attributes
class(a)



# There are also factors - but we will dive into those later, they are categorical
# Oh and lists... probably the most dynamic object in R

# These classes are default by what R interprets them to be, but we can change them

class(c)
# Other class changing functions
as.numeric()
as.integer()
as.POSIXct() # A Date Format
as.factor()
as.list()
as.data.frame() # An incredibly useful object

# Lets check the lengths of the variables

length(a)

length(c)
length(false)

# Other nice functions to create objects, seq(), matrix(), vector(), array()

print(n) 
# Lets look at the dimensions

# Lets also reset some of the variables
# The format for pulling values is [row, column]

print(n)

# We can be even more efficient, say all of column 1 is 3

print(n)

# Next lets turn the matrix into a dataframe - this will allow multiple classes

print(n)
class(n)


# Another useful tool is to bind datasets with rbind() and cbind()
a = 1:5
b = 20:24


d = cbind(a,b)
# These must be of equal length along the dimension you wish to bind
# Sam rows/same columns

# Other useful functions to mention, which(), mean(), sd(), min(), max()
 # Note the two equal signs that are necessary to check for equivalence
print(row)

mean(d)
sd(d)
min(d)
max(d)

# For and IF statements, basic structure and example
#sturcure
for (i in 1:5){ # How much it should loop, and the values
  # Input what you want it to do
}

if ( i == 5){ # Let it know what to check for
  # What you want to do if condition is met
}


# Example


print(d)


# Writing and Reading data files

# Working directories
# Lets find where you are
getwd()

# Now we can set the directory we want, you must have R point to the directory
# that you either want to save to or read from
#setwd("path")

# PRO TIP the path . gives you the current, and .. gives you the one before
# Say I need to go two folders back and up one to data, I would enter
# setwd("../../data) 
# Or if data was the next folder in from where I was setwd("./data)

# Set the working directory where you want
setwd(".") 


# Lets write a .csv



# Now lets read it back in


# Notice there are now rownames in the object
# You can avoid this with

# Notice that as you type a ton of options appear for specifying your exact need

# Other ways to read in data
read.table()
read.delim("blah.txt", header = TRUE, sep = "\t") # For tab delimited files


# Packages
# One of the coolest things in R is that it is open sourced and you can 
# download new analysis techniques from many authors on the fly and incorporate
# it into your work

# There are two steps, install and library



??ggplot2  # This allows us to check out the vignettes, or long form documentation




