
# install.packages("testthat")
# library(testthat)


context("Key Value Generator")

test_that("Proper key value pairs are constructed.", {
    expect_equal(key_value_generator("key")("value"),"--key=value")
    expect_equal(options$nodes("3"), "--nodes=3")
    expect_equal(options$nodes(3), "--nodes=3")
    expect_equal(options$memory("16g"), "--memory=16g")
})


test_that("Testing for errors and warnings.", {
    expect_error(throw_something())
    expect_warning(produce_warning(-3))
})