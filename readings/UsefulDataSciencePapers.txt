Useful Data Science Papers
------------------------

David Kahle, and Hadley Wickham. “Ggmap: Spatial Visualization with ggplot2.” R Journal 5, no. 1 (June 2013): 144–61. http://search.ebscohost.com/login.aspx?direct=true&db=a2h&AN=90616116&site=eds-live.

Hadley Wickham. “A Layered Grammar of Graphics.” Journal of Computational and Graphical Statistics 19, no. 1 (January 1, 2010): 3–28. doi:10.1198/jcgs.2009.07098.

John W. Emerson, Walton A. Green, Barret Schloerke, Jason Crowley, Dianne Cook, Heike Hofmann, and Hadley Wickham. “The Generalized Pairs Plot.” Journal of Computational and Graphical Statistics 22, no. 1 (January 1, 2013): 79–91. doi:10.1080/10618600.2012.694762.

Robert E. Kass, Brian S. Caffo, Marie Davidian, Xiao-Li Meng, Bin Yu, and Nancy Reid. “Ten Simple Rules for Effective Statistical Practice.” PLOS Comput Biol 12, no. 6 (June 9, 2016): e1004961. doi:10.1371/journal.pcbi.1004961.
 
